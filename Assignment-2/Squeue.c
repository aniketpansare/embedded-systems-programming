/** Squeue Device Driver -by Aniket Pansare **/
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include<linux/fs.h>
#include "hrt_driver.h"
#include <plat/dmtimer.h>
#include <linux/semaphore.h>

#define DRIVER_NAME "Squeue"
#define DEVICE_QUEUE1 "Squeue1"
#define DEVICE_QUEUE2 "Squeue2"
#define NAME_SIZE 20
#define QUEUE_SIZE 10

/*Semaphore to achieve synchromization.*/
struct semaphore sem1,sem2;

/** Function declarations **/
static int squeue_init(void);
static void squeue_exit(void);
static int squeue_open(struct inode *, struct file *); 
static ssize_t squeue_read(struct file *, char *, size_t, loff_t *);
static ssize_t squeue_write(struct file *, const char *, size_t, loff_t *);
static int squeue_release(struct inode *, struct file *);

/*Token structure*/
struct Token {
	int token_id;
	unsigned int timestamp[4];
	char str[80];
};

/* Device structure */
struct My_dev {
	struct cdev cdev;               /* The cdev structure */
	char name[NAME_SIZE];           /* Name of device*/
	struct Token *queue[QUEUE_SIZE];/* Queue of dynamically allocated tokens. */
	int head;			/* Head of the queue */
	int tail;			/* Tail of the queue */
	int count;
} *my_device[2];

static dev_t my_dev_number;      /* Allotted device number */
struct class *my_dev_class[2];      /* Tie with the device model */


/** file_operations structure **/
static struct file_operations fops = {
        .read = squeue_read,
        .open = squeue_open,
        .write = squeue_write,
	.release = squeue_release,
};

/** Function to Initailize the Driver **/
static int squeue_init(void) {
	int ret,i;

	/*Initialize semaphore for device 1*/
	sema_init(&sem1,1);
	/*Initialize semaphore for device 2*/
        sema_init(&sem2,1);
	
	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&my_dev_number, 0, 2, DRIVER_NAME) < 0) {
			printk(KERN_DEBUG "Can't register device\n"); return -1;
	}

	for(i=0;i<2;i++)
	{
		char device_name[20];
		/*Set Device Name*/
		sprintf(device_name, "%s%d",DRIVER_NAME,i+1);
		 
		/* Populate sysfs entries */
	        my_dev_class[i] = class_create(THIS_MODULE, device_name);

		 /* Allocate memory for the per-device structure */
	        my_device[i] = kmalloc(sizeof(struct My_dev), GFP_KERNEL);

        	if (!my_device[i]) {
                	printk("Bad Kmalloc\n"); return -ENOMEM;
        	}


		/* Initialize per device struture variables */
		sprintf(my_device[i]->name, "%s",device_name);
		my_device[i]->head=0;
		my_device[i]->tail=0;
		my_device[i]->count=0;

		/* Connect the file operations with the cdev */
		cdev_init(&my_device[i]->cdev, &fops);
		my_device[i]->cdev.owner = THIS_MODULE;

		/* Connect the major/minor number to the cdev */
		ret = cdev_add(&my_device[i]->cdev,MKDEV(MAJOR(my_dev_number),i),1);

		if (ret) {
			printk("Device could not be added.\n");
			return ret;
		}

		/* Send uevents to udev, so it'll create /dev nodes */
	        device_create(my_dev_class[i], NULL, MKDEV(MAJOR(my_dev_number), i), NULL, device_name);
	}

	printk("My Driver Initialized: Device1=%s Device2=%s.\n",my_device[0]->name,my_device[1]->name);
	return 0;

}

/** Exit Driver **/
static void squeue_exit(void) {
	int i;
	/* Release the major number */
	unregister_chrdev_region((my_dev_number), 2);

	/* Destroy device */
	for(i=0 ; i<2; i++)
	{
		device_destroy (my_dev_class[i], MKDEV(MAJOR(my_dev_number), i));
		cdev_del(&my_device[i]->cdev);
		kfree(my_device[i]);
	
		/* Destroy driver_class */
		class_destroy(my_dev_class[i]);
	}
	printk("Squeue Driver removed.\n");
}

/** Open the Driver **/
static int squeue_open(struct inode *inode, struct file *file) {
	struct My_dev *my_devp;

	/* Get the per-device structure that contains this cdev */
	my_devp = container_of(inode->i_cdev, struct My_dev, cdev);

	/* Easy access to cmos_devp from rest of the entry points */
	file->private_data = my_devp;

	printk("\n%s is opening\n", my_devp->name);
	return 0;
}

/*
 * Release the driver
 */
static int squeue_release(struct inode *inode, struct file *file)
{
	struct My_dev *my_devp = file->private_data;
	
	printk("\n%s is closing\n", my_devp->name);
	
	return 0;
}


/** Read from the Driver **/
static ssize_t squeue_read(struct file *file,char *buff,size_t len,loff_t *offset) {
	int res=0;
	struct My_dev *my_devp = file->private_data;
	struct semaphore *semptr;

	if(!strcmp(my_devp->name,DEVICE_QUEUE1))
	{
		semptr=&sem1; //Device 1
	}
	else
	{
		semptr=&sem2; //Device 2
	}

	/*Critical Section.. lock semaphore*/
	down(semptr);
	//printk(KERN_INFO "\n%s: dequeue()\n",my_devp->name);

	if(my_devp->count ==0)
		{
			printk(KERN_INFO "\n%s: Queue is empty\n",my_devp->name);
			up(semptr); /*release semaphore*/
			return -1;
		}
	
	/*capture dequeue timestamp*/
	my_devp->queue[my_devp->head]->timestamp[2]=omap_dm_timer_read_counter(my_timerp);

	/*write token to user space*/
	res=copy_to_user((void __user *)buff, (void *)my_devp->queue[my_devp->head], sizeof(struct Token));		

	/*Free dequeued Token*/
	kfree(my_devp->queue[my_devp->head]);

	/*set head in circular queue*/
	my_devp->head = (my_devp->head + 1)% QUEUE_SIZE;
	my_devp->count--;

	/*release semaphore*/
	up(semptr);
	return res;
}


/** Write to the Driver **/
static ssize_t squeue_write(struct file *file,const char *buff,size_t len,loff_t *off) {
	int res=0;
        struct My_dev *my_devp = file->private_data;
	struct semaphore *semptr;

	
	if(!strcmp(my_devp->name,DEVICE_QUEUE1))
        {
                semptr=&sem1;
        }
        else
        {
                semptr=&sem2;
        }
	
	/*Critical section.. lock semaphore.*/
	down(semptr);

	//printk(KERN_INFO "\n%s: enqueue()\n",my_devp->name);

	if(my_devp->count == QUEUE_SIZE)
		{
			printk(KERN_INFO "\n%s: Queue is Full\n",my_devp->name);
			up(semptr); /*Release semaphore*/
			return -1;
		}

	/*Allocate memory for token to be added to the queue.*/
	my_devp->queue[my_devp->tail]=kmalloc(sizeof(struct Token), GFP_KERNEL);

        if (!my_devp->queue[my_devp->tail]) {
                printk("Bad Kmalloc\n"); return -ENOMEM;
       		return -1;
	}

	/*Copy token sent by user program to kernel space.*/
	res=copy_from_user((void *)my_devp->queue[my_devp->tail], (void __user *)buff, sizeof(struct Token));


	/*Capture enqueue timestamp in driver and save it to the token.*/
	my_devp->queue[my_devp->tail]->timestamp[1]=omap_dm_timer_read_counter(my_timerp);

	/*set tail in circular queue*/
	my_devp->tail = (my_devp->tail + 1) % QUEUE_SIZE;
	my_devp->count++;
			
	up(semptr); /*Release semaphore*/
	return res;
}

module_init(squeue_init);
module_exit(squeue_exit);

MODULE_AUTHOR("Aniket Pansare");
MODULE_DESCRIPTION("Character Decice Driver");
MODULE_LICENSE("GPL");
