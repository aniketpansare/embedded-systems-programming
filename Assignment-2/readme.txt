#### Readme.txt for ESP Assignment-2 ####

A) Question 1: Can you find the configuration parameter that defines the default rate for jiffies?
	The HZ variable/parameter in jiffies.h file defines the default rate for jiffies.

B) Steps to Execute the Program:
	1. Copy HRT.ko, Squeue.ko and main_2 file to BeagleBoard.
	2. insmod HRT.ko
	3. insmod Squeue.ko
	4. ./main_2 (If permission denied)	
			a. Execute: chmod +x main_2 
			b. ./main_2

C) Steps to compile:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1.
	2. Build the kernel source and install the toolchain.
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

D) Step to clean the build Files:
	1. make clean