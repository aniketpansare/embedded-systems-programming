/** Test progrom for Squeue Driver - By Aniket Pansare **/
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <semaphore.h>
#include <time.h>
#include <math.h>

/*Define ioctl options*/
#define START_TIMER 0xffc1
#define STOP_TIMER 0xffc2
#define SET_TIMER 0xffc3

/* Max tokens sent by each sender and no of senders*/
#define MAX_SENT_TOKEN 100
#define NO_OF_SENDER 6

/*File descriptors.*/
int squeue1_fd,squeue2_fd,hrt_fd;

/*Global counter to assign unique token_id*/
int token_id_count=1;

/*Semaphore to assign unique token_id's*/
sem_t sem;

/*Token Structure*/
struct Token {
        int token_id;
        unsigned int timestamp[4];
        char str[80];
};

/*Print the token*/
void printToken(struct Token *token)
{
	printf("\n\nToken_id: %d \nTimestamp[0]: %u \nTimestamp[1]: %u \nTimestamp[2]: %u \nTimestamp[3]: %u \nString: %s",token->token_id,token->timestamp[0],token->timestamp[1],token->timestamp[2],token->timestamp[3],token->str);
}

/*Send tokens to Squeue1.. Enqueue Operation*/
void * sender1()
{
	
	int count,sleep_time,res;
	for(count=0; count < MAX_SENT_TOKEN; )
	{
		struct Token token;
		/*lock semaphore*/
		sem_wait(&sem);	
		token.token_id= token_id_count++;

		/*Read HRT Time stamp before enqueue*/
		read(hrt_fd,(char *)&token.timestamp[0], sizeof(unsigned int));
		sprintf(token.str,"Token form Squeue1 with id %d",token.token_id);		

		/*Token enqueue in Squeue1*/
		res=write(squeue1_fd,(char *)&token, sizeof(struct Token));
		if(res==-1)
		{	
			token_id_count--;
			sem_post(&sem);
			// Sleep if Queue is FULL.
                        srand(time(NULL));
                        sleep_time=rand();
                        usleep(((sleep_time%10)+1)*1000);
			continue;
		}
		else
		{
			count++;
		}
		/*Release semaphore*/
                sem_post(&sem);

		srand(time(NULL));
		sleep_time=rand();
		usleep(((sleep_time%10)+1)*1000);
	}	
}

/*Send tokens to Squeue2.. Dequeue Operation*/
void * sender2()
{
        int count,sleep_time,res;
        for(count=0; count < MAX_SENT_TOKEN; )
        {
        	struct Token token;
		/*lock semaphore*/
		sem_wait(&sem);
		token.token_id= token_id_count++;	
        	/*Read HRT timestamp before enqueue*/
		read(hrt_fd,(char *)&token.timestamp[0], sizeof(unsigned int));
		sprintf(token.str,"Token form Squeue2 with id %d",token.token_id);	

		/*Token enqueue in Squeue2*/
		res=write(squeue2_fd,(char *)&token, sizeof(struct Token));
		
		if(res==-1)
                {
                        token_id_count--;
                        sem_post(&sem);
                        // Sleep if Queue is FULL.
                        srand(time(NULL));
                        sleep_time=rand();
                        usleep(((sleep_time%10)+1)*1000);
                        continue;
                }
                else
                {
                        count++;
                }       
	
		//Release Semaphore
		sem_post(&sem);
        	
		srand(time(NULL));
                sleep_time=rand();
                usleep(((sleep_time%10)+1)*1000);
	}
}

/*Read tokens from Squeue1 and Squeue2*/
void * receiver()
{	int res1 =0, res2=0;
	int read_count=0,sleep_time;

	/*Loop until all sent tokens are read.*/
	while(read_count < NO_OF_SENDER * MAX_SENT_TOKEN)
	{

		struct Token token;
		/*Read tokens from Squeue1*/
		while(1)
		{	
			res1 = read(squeue1_fd,(char *)&token, sizeof(struct Token));
			if(res1 == -1) break;
			res2 = read(hrt_fd,(char *)&token.timestamp[3], sizeof(unsigned int));
			if(res2 != 0) {printf("\n### Failed to read HRT timestamp. ###"); return;}
			printToken(&token);
			read_count ++;
		}
		/*Read tokens from Squeue2*/	
		while(1)
		{
			res1 = read(squeue2_fd,(char *)&token, sizeof(struct Token));
                	if(res1 == -1) break;
			res2 = read(hrt_fd,(char *)&token.timestamp[3], sizeof(unsigned int));
			if(res2 != 0) {printf("\n### Failed to read HRT timestamp. ###"); return;}
			printToken(&token);
			read_count++;
		}
		srand(time(NULL));
                sleep_time=rand();
                usleep(((sleep_time%10)+1)*1000);
	}

}


int main(int argc, char *argv[])
{
	int i=0;
	/*Define 6 sender and 1 receiver thread*/
	pthread_t p_send[NO_OF_SENDER];
	pthread_t p_recv;
	
	/*Initialize semaphore*/
	sem_init(&sem,0,1);

	 /* open devices */      
       	hrt_fd = open("/dev/MY_HRT_TIMER", O_RDWR);
	squeue1_fd = open("/dev/Squeue1",O_RDWR);
	squeue2_fd = open("/dev/Squeue2",O_RDWR);

       	if (squeue1_fd<0 || squeue2_fd<0 || hrt_fd<0 )
       	{
               	printf("Can not open device file.. Please check if ran with sudo\n");
               	return 0;
       	}

	/*Initialize HRT Timer*/
	ioctl(hrt_fd,START_TIMER);
	ioctl(hrt_fd,SET_TIMER,(unsigned int) 0);

	/*Create sender threads for Squeue1*/
	for(i=0; i<NO_OF_SENDER/2 ; i++)
	{
		pthread_create(&p_send[i],NULL,&sender1,NULL);
	}

	/*Create sender threads for Squeue2*/
	for(i=NO_OF_SENDER/2; i<NO_OF_SENDER ; i++)
	{
		 pthread_create(&p_send[i],NULL,&sender2,NULL);
	}

	/*Create receiver thread*/
	pthread_create(&p_recv,NULL,&receiver,NULL);

	/*Wait for sender threads to exit*/	
	for(i=0; i<NO_OF_SENDER; i++)
	{
		pthread_join(p_send[i], NULL);
	}

	/*Wait for receiver thread to exit*/
	pthread_join(p_recv,NULL);

	/*Stop Timer*/
	ioctl(hrt_fd,STOP_TIMER);

	/* close devices */
	close(squeue1_fd);
	close(squeue2_fd);
	close(hrt_fd);
	return 0;
}

