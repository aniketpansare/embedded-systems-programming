/** High Resolution Timer Device Driver -by Aniket Pansare **/
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include<linux/fs.h>
#include<plat/dmtimer.h>


#define DEVICE_NAME "MY_HRT_TIMER"
#define NAME_SIZE 20
#define START_TIMER 0xffc1 
#define STOP_TIMER 0xffc2
#define SET_TIMER 0xffc3

/** Function declarations **/
static int hrt_init(void);
static void hrt_exit(void);
static int hrt_open(struct inode *, struct file *); 
static ssize_t hrt_read(struct file *, char *, size_t, loff_t *);
static int hrt_release(struct inode *, struct file *);
long hrt_ioctl(struct file *, unsigned int, unsigned long);

/** Symbol for HRT timer to be used by other modules **/
struct omap_dm_timer *my_timerp;
EXPORT_SYMBOL(my_timerp);

/* Device structure */
struct My_dev {
	struct cdev cdev;               /* The cdev structure */
	char name[NAME_SIZE];           /* Name of device*/
} *my_devp;

static dev_t my_dev_number;      /* Allotted device number */
struct class *my_dev_class;      /* Tie with the device model */

/** file_operations structure **/
static struct file_operations fops = {
        .read = hrt_read,
        .open = hrt_open,
	.release = hrt_release,
	.unlocked_ioctl = hrt_ioctl,
};

/** Function to Initailize the Driver **/
static int hrt_init(void) {
	int ret;

	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&my_dev_number, 0, 1, DEVICE_NAME) < 0) {
			printk(KERN_DEBUG "Can't register device\n"); return -1;
	}

	/* Populate sysfs entries */
	my_dev_class = class_create(THIS_MODULE, DEVICE_NAME);

	
	/* Allocate memory for the per-device structure */
	my_devp = kmalloc(sizeof(struct My_dev), GFP_KERNEL);


	if (!my_devp) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}

	/* Request I/O region */
	sprintf(my_devp->name, DEVICE_NAME);

	/*Reserve HRT Timer*/
	my_timerp = omap_dm_timer_request();

        if(my_timerp == NULL)
        {	/*No Unreserved HRT timers available*/
                printk("\n Timer Request Failed.. No unreserved timers");
                return -1;
        }

	/*Set timer Source*/
	omap_dm_timer_set_source(my_timerp,0);

	 /*Enable dm timer.*/
        omap_dm_timer_enable(my_timerp);

	/* Connect the file operations with the cdev */
	cdev_init(&my_devp->cdev, &fops);
	my_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
	ret = cdev_add(&my_devp->cdev, (my_dev_number), 1);

	if (ret) {
		printk("Bad cdev\n");
		return ret;
	}

	/* Send uevents to udev, so it'll create /dev nodes */
	device_create(my_dev_class, NULL, MKDEV(MAJOR(my_dev_number), 0), NULL, DEVICE_NAME);		
	

	printk("My HRT Driver Initialized.\n");
	return 0;

}

/** Exit Driver **/
static void hrt_exit(void) {

	omap_dm_timer_stop(my_timerp); /*Stop HRT Timer*/
	omap_dm_timer_disable(my_timerp); /*Free HRT Timer*/
	omap_dm_timer_free(my_timerp);  /*Free HRT Timer*/

	/* Release the major number */
	unregister_chrdev_region((my_dev_number), 1);

	/* Destroy device */
	device_destroy (my_dev_class, MKDEV(MAJOR(my_dev_number), 0));
	cdev_del(&my_devp->cdev);
	kfree(my_devp);
	
	/* Destroy driver_class */
	class_destroy(my_dev_class);

	printk("HRT Driver removed.\n");
}

/** Open the Driver **/
static int hrt_open(struct inode *inode, struct file *file) {
	struct My_dev *my_devp;

	/* Get the per-device structure that contains this cdev */
	my_devp = container_of(inode->i_cdev, struct My_dev, cdev);

	/* Easy access to cmos_devp from rest of the entry points */
	file->private_data = my_devp;

	printk("\n%s is opening\n", my_devp->name);
	return 0;
}

/*
 * Release the driver
 */
static int hrt_release(struct inode *inode, struct file *file)
{
	struct My_dev *my_devp = file->private_data;
	
	printk("\nClosing %s File descriptor \n", my_devp->name);
	
	return 0; 
}


/** Read from the Driver **/
static ssize_t hrt_read(struct file *fp,char *buff,size_t len,loff_t *offset) {
	int res;
	unsigned int counter = 0;	
	/*Read the timer value and return*/
	counter = omap_dm_timer_read_counter(my_timerp);

	res = copy_to_user((void __user *)buff, (void *)&counter, sizeof(unsigned int));

	return res; /*On success return 0*/
}


/* ioctl calls to START, STOP and SET the timer.*/
long hrt_ioctl(struct file *file,	
		 unsigned int ioctl_num,	/* number and param for ioctl */
		 unsigned long ioctl_param) {
	/* 
	 * Switch according to the ioctl called 
	 */
	switch (ioctl_num) {
	case START_TIMER:
		omap_dm_timer_start(my_timerp); /*START the timer*/
		break;


	case STOP_TIMER:
		omap_dm_timer_stop(my_timerp); /*STOP the timer*/
		break;

	case SET_TIMER:
		omap_dm_timer_write_counter(my_timerp, (unsigned int) ioctl_param); /*SET the timer*/
		break;
	}

	return 0;
}

module_init(hrt_init);
module_exit(hrt_exit);

MODULE_AUTHOR("Aniket Pansare");
MODULE_DESCRIPTION("High Resolution Timer Decice Driver");
MODULE_LICENSE("GPL");
