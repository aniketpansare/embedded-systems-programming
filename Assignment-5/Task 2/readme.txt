######### Assignment-5 (Task 2)################
	
Tested on : QEMU

Note: Both Task-1 and Task-2 contain same code with different printf functionality.
	  However: Task 1 does everything using printf logging functionality (USE_DEFAULT_PRINTF = 0)
				Task 2 does everything using default printf. (USE_DEFAULT_PRINTF = 1)

	
Task 2:
	Changes Made in Below Files:
		1. /FreeRTOS2/Source/tasks.c   (Line No:427 to Line No:500)
				a) Added API to create Real-time Task.
				b) Added Real-time timer callback method.
				
 		2. /FreeRTOS2/Demo/SampleTasks/SampleTasks.c (Line No:113 to Line No:138)
				- Added Real-time Task function to execute.
				
		3. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/main.c 
				- Added code to create 10 real-time tasks, 1 critical and 1 lower priority task.
				
		4. /FreeRTOS2/Demo/Console/include/my_logger.h
				- Declared Global structures and Config parameters in one single .h file.
		
		5. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/FreeRTOSConfig.h
				- Modified config parameter to allow 10 timers in FreeRTOS.
				
		6. /FreeRTOS2/Source/include/task.h
				- Declared Real-time Task creation API in this header file.
				

Approach:
	1. Created 10 Real-time tasks and associated a timer with each task.
	2. All real time tasks assigned priority 2.
	3. Created one critical task with priority 3 and one lower priority task with priority 1.
	4. Maintained a global Real-time timer task struct to keep track of all task deadlines.
	5. Added head and tail parameters to keep track of task overrun in-case unable to meet deadline.
	6. Timer used to poke each task at the start of its time period.
	7. Real time task suspended after finishing its execution and waits for next start period.
	8. Task with the highest priority executed at any time using default scheduling.
	9. If real time tasks with same priority arrive then task with earliest deadline executed first.
	
Steps to compile and Execute:
		1. Go to /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC
		2. export PATH=/home/esp/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_EABI/bin/:$PATH
		3. execute: make clean.
		4. execute: make
		5. Go to path containing QEMU Image (beagle_sd.img)
		6. Execute: sudo mount -o loop,offset=$[63*512] beagle_sd.img /mnt/
		7. Execute: sudo cp /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/rtosdemo.bin /mnt/
		8. Execute: sudo umount /mnt.
		9. Execute: qemu-system-arm -M beaglexm -sd ./beagle_sd.img -clock unix -serial stdio
					mmc rescan 0
					fatload mmc 0 80300000 rtosdemo.bin
					go 0x80300000