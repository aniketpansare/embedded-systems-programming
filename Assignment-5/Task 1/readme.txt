######### Assignment-5 (Task-1)################

Tested on : QEMU

Task 1:
	Changes are Made to Below Files:
		1. /FreeRTOS2/Source/portable/GCC/OMAP3_BeagleBoard/portISR.c
				- Added Interrupt handler for UART.
			
		2. /FreeRTOS2/Demo/Console/my_logger.c
				- New file created for logging printf messages.
				- Added support for handling different log levels.
						
		3. /FreeRTOS2/Demo/Console/include/my_logger.h
				- New file created to manage all declarations and config parameters.
		
		4. /FreeRTOS2/Demo/Console/printf.c
				- Modified slightly to manage both regular printf and Logging functionality.
				- Config Parameter defined in my_logger.h
		
		5. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/main.c
				- Added printf statements with diffrent Log Levels.
		
		6. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/Makefile
				- Modified slightly to handle newly added files.


Approach:
	1. Config Parameter "USE_DEFAULT_PRINTF" used to enable/disable logging functionality.
	2. Defined DEFAULT_MESSAGE_LOG_LEVEL and DEFAULT_CONSOLE_LOG_LEVEL config paramters.
	3. If logging functionality is enabled all printf messages will be stored in a log buffer and printf function will return immediately.
	4. UART Interrupt is enabled and character is printed on each interrupt.
	5. No Busy wait performed.
	6. Interrupt enable-disable and printf with logging enabled code added to my_logger.c file.				
		
Steps to compile and Execute:
		1. Go to /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC
		2. export PATH=/home/esp/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_EABI/bin/:$PATH
		3. execute: make clean.
		4. execute: make
		5. Go to path containing QEMU Image (beagle_sd.img)
		6. Execute: sudo mount -o loop,offset=$[63*512] beagle_sd.img /mnt/
		7. Execute: sudo cp /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/rtosdemo.bin /mnt/
		8. Execute: sudo umount /mnt.
		9. Execute: qemu-system-arm -M beaglexm -sd ./beagle_sd.img -clock unix -serial stdio
					mmc rescan 0
					fatload mmc 0 80300000 rtosdemo.bin
					go 0x80300000


