/**
* ESP Assignment-4 Task-3
* Aniket Pansare
**/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <semaphore.h>
#include <linux/input.h>
#include <sys/select.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>

#define MOUSE_FILE "/dev/input/mouse_aniket0"
#define MAX_THREAD 3
#define BUTTON 273
#define MOD_VAL 10000000
uint64_t MAX_LIMIT = 1000000000;

uint64_t result[100];
volatile sig_atomic_t signal_caught = 0;
int fd =0;

/**
* Custom signal handler.
*/
void signal_func(int signal)
{
	signal_caught = 1;  // Set sig atomic variable when signal caught.
	
	printf("\nInside Signal Handler");
}

/**
* Imprecise Computation.
* Calculate n*(n+1)/2 for large values of n.
*/
uint64_t * impreciseComputation()
{
	uint64_t i=0,k=0,carry=0,temp=0;

	for(k=1; k < MAX_LIMIT ; k++)
	{
		i=0;
		carry =  (result[i] + k)/MOD_VAL;
		result[i] = (result[i] + k)%MOD_VAL;
		i++;
		while(carry != 0)
		{
		temp = (result[i] + carry)/MOD_VAL;
		result[i] = (result[i] + carry)%MOD_VAL;		

		carry = temp;
		i++;
		}

		 if (signal_caught == 1)      //Exit with Intermediate result when signal received.
                {
                        printf("\nSIGTERM : Signal Caught (Returning Computed Value)\n");
		        signal_caught = 0;
			break;
                }
	}	

	return result;
}

/**
* Main Program.
*/
int main()
{
	int i=0,flag=0;
	pthread_t thread1;
	
	fd=open(MOUSE_FILE,O_RDONLY); // Open mouse_aniket0 .. 

        if(fd == -1)
        {
                printf("\n!!! Failed to open mouse device.. (Try executing with root privilege) !!!\n");
                exit(-1);
        }

	signal(SIGTERM, signal_func);   //Register Signal Handler

	for(i=0;i<10;i++)
	{
		result[i]=0;
	}

	impreciseComputation();             // Start Computation.

	printf("\nComputation Output: ");   //Print Computed Output.	
	for(i=99;i>=0;i--)                  
	{

		if(flag == 1)
                        {
                                printf("%07llu",result[i]);
                        }
	
		if(result[i] != 0 && flag ==0)
			{	
				printf("%llu",result[i]);
				flag =1;
			}
	}
	printf("\n");
	return 0;
}
