######### Assignment-4 (Task-3) (Part-2) #####################

Kernel Driver Approach:
	1. I edited the mousedev.c driver program and created my own customized driver my_mousedev.c.
	2. This driver will look for double click events and raise a SIGTERM signal.
	3. The signal will be delivered to the userspace program which has opened my driver.
	
	User Program:
	1. User program opens a device "/dev/input/mouse_aniket0" .(created after installing driver) 
	2. User program also has a ImpreciseComputation function which will give addition output of 1 to n numbers.
	3. A signal handler for SIGTERM is registerd in main program.	
	4. Whenever a double click event is received SIGTERM signal is raised by the driver. 
	3. The computation will stop and intermediate output will be printed.


A) Steps to Execute the Program:
	1. Copy the main_4 and my_mousedev.ko files in Task3/Part2 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. insmod my_mousedev.ko
	3. ./main_4
				You will get a Computation Output after Double Click. 
	4. rmmod my_mousedev.ko				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
