/**
* ESP Assignment-4 Task-3
* Aniket Pansare
**/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <semaphore.h>
#include <linux/input.h>
#include <sys/select.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/syscall.h>
#include <signal.h>
#include <fcntl.h>

#define MOUSE_FILE "/dev/input/event1"		  //Mouse device to monitor
#define BUTTON 273 				  //Right Mouse Button
#define MOD_VAL 10000000
uint64_t MAX_LIMIT = 1000000000;

uint64_t result[100];
volatile sig_atomic_t signal_caught = 0;
int fd =0;

/**
* Custom signal handler.
*/
void signal_func(int signal)
{
	signal_caught = 1;  // Set sig atomic variable when signal caught.
	
	printf("\nInside Signal Handler");
}

/**
* Imprecise Computation.
* Calculate n*(n+1)/2 for large values of n.
*/
uint64_t * impreciseComputation()
{
	uint64_t i=0,k=0,carry=0,temp=0;

	for(k=1; k < MAX_LIMIT ; k++)
	{
		i=0;
		carry =  (result[i] + k)/MOD_VAL;
		result[i] = (result[i] + k)%MOD_VAL;
		i++;
		while(carry != 0)
		{
		temp = (result[i] + carry)/MOD_VAL;
		result[i] = (result[i] + carry)%MOD_VAL;		

		carry = temp;
		i++;
		}

		 if (signal_caught == 1)      //Exit with Intermediate result when signal received.
                {
                        printf("\nSIGTERM : Signal Caught (Returning Computed Value)\n");
		        signal_caught = 0;
			break;
                }
	}	

	return result;
}


/**
* Thread to monitor double click events.
*/
void * thread_func()
{
	int res,no_events,i;
        int flag_doublclick=0;
        int click_count=0;
        double time1=0.0,time2=0.0;
        struct input_event input,click[2];
        fd_set readfd_set;

	while(1){
                FD_ZERO(&readfd_set);
                FD_SET(fd,&readfd_set);

                select(fd + 1, &readfd_set, NULL, NULL, NULL); //Check if any event available for read.

                if ( FD_ISSET(fd, &readfd_set) ) {

                        if(click_count < 1)                  //Capture first click.
                        {
                                res=read( fd, &click[0], sizeof(struct input_event));
                                if(click[0].code == BUTTON && click[0].value==1)
                                {
                        		click_count++;
				}
				else{
                                        FD_CLR(fd,&readfd_set);   //If it is not a right click (IGNORE)
                                        continue;
                                }
                        }
                        else					//Capture second click.
                        {
                                res=read( fd, &click[1], sizeof(struct input_event));
                                 if(click[1].code == BUTTON && click[1].value==1)
                                {

                                	time1 =( ((double)click[0].time.tv_sec) + (((double)click[0].time.tv_usec)/1000000.0));
					time2 =( ((double)click[1].time.tv_sec) + (((double)click[1].time.tv_usec)/1000000.0));

                                	if((time2 - time1) < 0.3)
                                	{
                                        	printf("\n!!! Double Click !!!\n");
						kill(syscall(SYS_gettid),SIGTERM);
                                		return;
					}
					else
					{
			   			memcpy(&click[0],&click[1],sizeof(struct input_event));
					}
                             
				}
				
                        }

                        FD_CLR(fd,&readfd_set);
                }


        }

}


/**
* Main Program.
*/
int main()
{
	int i=0,flag=0;
	pthread_t thread1;
	
	fd=open("/dev/input/event1",O_RDONLY); // Open Mouse .. (event1) to read.
        if(fd == -1)
        {
                printf("\n!!! Failed to open mouse device..\n1.Try executing with root privilege OR\n2.Check if mouse at /dev/input/event1) !!!\n");
                exit(-1);
        }

	signal(SIGTERM, signal_func);   //Register Signal Handler

	pthread_create(&thread1,NULL,thread_func,NULL);  //Create thread to monitor Double Click.

	for(i=0;i<10;i++)
	{
		result[i]=0;
	}

	impreciseComputation();             // Start Computation.

	printf("\nComputation Output: ");   //Print Computed Output.	
	for(i=99;i>=0;i--)                  
	{

		if(flag == 1)
                        {
                                printf("%07llu",result[i]);
                        }
	
		if(result[i] != 0 && flag ==0)
			{	
				printf("%llu",result[i]);
				flag =1;
			}
	}
	printf("\n");

	pthread_join(thread1,NULL); 	
	close(fd);
	return 0;
}
