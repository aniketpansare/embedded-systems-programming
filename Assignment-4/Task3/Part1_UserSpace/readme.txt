######### Assignment-4 (Task-3) #####################

Userspace Approach:
	1. I have created a ImpreciseComputation function which will give addition output of 1 to n numbers.
	2. Then, I created a thread program which checks for a double click event from Userspace.
	3. A signal handler for SIGTERM is registerd in main program.
	4. Whenever a double click event is received SIGTERM signal is raised. 
	3. The computation will stop and intermediate output will be printed.


A) Steps to Execute the Program:
	1. Copy the main_4 file in Task3 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. ./main_4
				You will get a Computation Output after Double Click. 
				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
