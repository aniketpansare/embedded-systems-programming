/**
* ESP Assignment-4 -> Task-4 -> Part-2
* Signal received by all threads.
**/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <semaphore.h>
#include <sys/syscall.h>

#define MAX 3

sem_t sem;
volatile sig_atomic_t signal_caught = 0;
pthread_t thread[MAX];
sigset_t *mask;

/**
* Custom signal handler.
*/
void signal_func(int signal)
{
	//Signal Caught
	signal_caught = syscall(SYS_gettid);
	
	printf("Signal Handler called in context of thread with id: %ld\n",syscall(SYS_gettid));
}

/**
* Thread function.
**/
void * thread_func(void * ptr)
{
	mask= ptr;
	int signal = 0, res =0,i=0;
	printf("Inside Thread with id: %ld\n",syscall(SYS_gettid));


	res = pthread_sigmask(SIG_UNBLOCK,mask,NULL);  //UnBlock Signal.
        if(res != 0)
        {
                printf("Error in setting pthread_sigmask\n");
                return ;
        }


	while(1)
	{
		sleep(1);
		if (signal_caught == syscall(SYS_gettid)) 
		{
			printf("Raising new Signal from thread with id: %ld\n\n",syscall(SYS_gettid));
			signal_caught = 0;
			
			pthread_sigmask(SIG_BLOCK,mask,NULL);
			
			kill(syscall(SYS_gettid),SIGIO); // Raise Signal for other running threads.
			return;  
		}
	}

}

/**
* Main Program.
**/
int main()
{
	int i=0,res=0;
	sigset_t mask;
	struct sigaction sa;

	printf("\nInside Main with id: %ld\n",syscall(SYS_gettid));

	sigemptyset(&sa.sa_mask);
	sigaddset(&sa.sa_mask,SIGIO);
	sa.sa_flags =0;
	sa.sa_flags = sa.sa_flags | SA_NODEFER;
	sa.sa_handler = signal_func;
	sigaction(SIGIO,&sa,NULL);


	sem_init(&sem,0,1);
   
	res = pthread_sigmask(SIG_BLOCK,&sa.sa_mask,NULL); // Block SIGIO Signal.
	if(res != 0)
	{
		printf("Error in setting pthread_sigmask\n");
		return -1;
	} 
	
	for(i=0;i<MAX;i++)  // Create Threads.
	{
		pthread_create(&thread[i],NULL,thread_func,&sa.sa_mask);
	}
	
	printf("Raising SIGIO signal from main to all threads..!!\n\n");
	sleep(1);

	kill(syscall(SYS_gettid),SIGIO);
		
	
	for(i=0;i<MAX;i++)
	{
		pthread_join(thread[i],NULL);
	}
	
	printf("\nAll threads received signal.. Exiting Program !!\n");	

	return 0;
}
