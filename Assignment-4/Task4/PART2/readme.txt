######### Assignment-4 (Task-4) (PART2) #####################

All Thread Signalling Approach:
	1. I have created 3 threads which can be increased as required.
	2. A SIGIO signal handler is registered in main.
	3. Then, I block the SIGIO signal in main and ensure that its unblocked in all the threads.
	4. The SIGIO signal is then raised from main using kill system call.
	5. This raised signal will be delivered to any arbitrary thread in the thread group for the process.
	6. Which ever thread receives the signal raises it again for all the remaining threads.
	7. I ensure that once a thread receives a signal it blocks itself on that signal.
	8. Eventually, all threads will receive the signal and exit.
	9. Thus, signal delivered to one thread is sent to all other threads.
	
	
A) Steps to Execute the Program:
	1. Copy the main_4 file in Task4/PART2 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. ./main_4				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
