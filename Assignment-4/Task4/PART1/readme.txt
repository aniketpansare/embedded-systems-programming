######### Assignment-4 (Task-4) (PART1) #####################

Arbitrary Thread Approach:
	1. I have created 3 threads which can be increased as required.
	2. Main program will raise the first SIGALRM signal.
	3. This signal will be consumed by any one of the threads arbitrarily.
	4. The thread consuming a signal raises a new signal.
	5. The newly raised signal will again be consumed by any arbitrary thread.
	6. This goes in infinite loop.
	
A) Steps to Execute the Program:
	1. Copy the main_4 file in Task4/PART1 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. ./main_4				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
