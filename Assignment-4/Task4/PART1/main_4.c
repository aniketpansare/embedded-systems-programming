/**
* ESP Assignment 4 - Task 4 - Part 1.
* Signal received by any arbitrary thread.
**/
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <semaphore.h>
#include <sys/syscall.h>

#define MAX 3

sem_t sem;
volatile sig_atomic_t signal_caught = 0;

/**
* Custom Signal Handler.
**/
void signal_func(int signal)
{
	//Signal Caught
	signal_caught = syscall(SYS_gettid) ;
	
	printf("\nSignal Handler called in context of thread with id: %ld",syscall(SYS_gettid));

}

/**
* Thread function.
**/
void * thread_func()
{
	printf("Inside Thread with id: %ld\n",syscall(SYS_gettid));
	
	while(1)
	{
		sleep(1);

		if (signal_caught == syscall(SYS_gettid))   //Check if signal handler invoed in context of this thread. 
		{
			printf("\nRaising new Signal from thread with id: %ld\n",syscall(SYS_gettid));
			signal_caught = 0;  //Reset condition.
			alarm(1);           // Raise new signal.
		}

	}

}

/**
* Main Program.
*/
int main()
{
	int i=0,res=0;
	pthread_t thread[MAX];
	sigset_t mask;

	sem_init(&sem,0,1);

	signal(SIGALRM, signal_func);//Register Signal Handler
	sigaddset(&mask,SIGALRM);		

	for(i=0;i<MAX;i++)
	{
		pthread_create(&thread[i],NULL,thread_func,NULL);
	}

	res = pthread_sigmask(SIG_BLOCK,&mask,NULL); //Block signal in Main Thread.
	
	alarm(1);  //Raise first signal after thread creation.

	for(i=0;i<MAX;i++)
	{
		pthread_join(thread[i],NULL);
	}

}
