/**
* ESP Assignment 4- Task 2.
* Printing Computation output after 1 second.
**/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include<semaphore.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#define MOD_VAL 10000000
uint64_t MAX_LIMIT = 1000000000;
uint64_t result[100];

volatile sig_atomic_t signal_caught = 0;

/**
* Custom signal handler.
*/
void signal_func(int signal)
{
	//Signal Caught
	signal_caught = 1;
	
	printf("\nInside Signal Handler");
}


/**
* Imprecise Computation.
* Calculate n*(n+1)/2 for large values of n.
*/
uint64_t * impreciseComputation()
{
        uint64_t i=0,k=0,carry=0,temp=0;

        for(k=1; k < MAX_LIMIT ; k++)
        {
                i=0;
                carry =  (result[i] + k)/MOD_VAL;
                result[i] = (result[i] + k)%MOD_VAL;
                i++;
                while(carry != 0)
                {
                temp = (result[i] + carry)/MOD_VAL;
                result[i] = (result[i] + carry)%MOD_VAL;

                carry = temp;
                i++;
                }

                 if (signal_caught == 1)      //Exit with Intermediate result when signal received.
                {
                        printf("\nSIGALRM : Signal Caught (Returning Computed Value)\n");
                        signal_caught = 0;
                        break;
                }
        }

        return result;
}


/**
* Main Program.
*/
int main()
{
	int i=0,flag=0;

	signal(SIGALRM, signal_func);//Register Signal Handler

	alarm(1);   			//Raise signal.

	impreciseComputation();	

	printf("\nComputation Output after 1 Second: "); //Print Computation output after 1 second.
	
	for(i=99;i>=0;i--)
        {

                if(flag == 1)
                        {
                                printf("%07llu",result[i]);
                        }

                if(result[i] != 0 && flag ==0)
                        {
                                printf("%llu",result[i]);
                                flag =1;
                        }
        }
        printf("\n");

	return 0;
}
