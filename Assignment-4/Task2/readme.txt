######### Assignment-4 (Task-2) #####################

Approach:
	1. I have created a ImpreciseComputation function which will give addition output of 1 to n numbers.
	2. I have registered a SIGALRM signal in main program.
	3. The signal will be raised after 1 second and intermediate output will be printed.


A) Steps to Execute the Program:
	1. Copy the main_4 file in Task2 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. ./main_4
				You will get a Computation Output after 1 Second. 
				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
