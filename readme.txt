######################################################################		
		#### Readme.txt for ESP Assignment-1 ####
######################################################################
A) Steps to Execute the Program:
	1. Copy the gmem.ko and the gmem_tester file to BeagleBoard.
	2. insmod gmem.ko
	3. chmod +x gmem_tester (optional if permision already present)
	4. ./gmem_tester show
	5. ./gmem_tester write "String to append"


B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

		
######################################################################		
		#### Readme.txt for ESP Assignment-2 ####
######################################################################

A) Question 1: Can you find the configuration parameter that defines the default rate for jiffies?
	The HZ variable/parameter in jiffies.h file defines the default rate for jiffies.

B) Steps to Execute the Program:
	1. Copy HRT.ko, Squeue.ko and main_2 file to BeagleBoard.
	2. insmod HRT.ko
	3. insmod Squeue.ko
	4. ./main_2 (If permission denied)	
			a. Execute: chmod +x main_2 
			b. ./main_2

C) Steps to compile:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1.
	2. Build the kernel source and install the toolchain.
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

D) Step to clean the build Files:
	1. make clean
	
######################################################################		
		#### Readme.txt for ESP Assignment-3 (Task 1) ####
######################################################################

A) Steps to Execute the Program:
	1. Copy the main_3 file in Task1 folder to BeagleBoard.
	2. chmod +x main_3 (optional if permision already present)
	3. ./main_3
				You will get a menu-based options.
				0: Write
						->Enter Number of Pages to write.
				1: Read
						->Enter Number of Pages to read.
				2: Seek
						->Enter Page number to seek.
	

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

		
######################################################################		
		#### Readme.txt for ESP Assignment-3 (Task 2) ####
######################################################################

A) Steps to Execute the Program:
	1. Copy the i2c_driver.ko and main_3 file in Task2 folder to BeagleBoard.
	2. insmod i2c_driver.ko
	2. chmod +x main_3 (optional if permision already present)
	3. ./main_3
				You will get a menu-based options.
				0: Write
						->Enter Number of Pages to write.
				1: Read
						->Enter Number of Pages to read.
				2: Seek
						->Enter Page number to seek.
	4. rmmod i2c_driver.ko


B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

		
######################################################################		
		#### Readme.txt for ESP Assignment-3 (Task 3) ####
######################################################################

A) Steps to Execute the Program:
	1. Copy the i2c_driver.ko and main_3 file in Task3 folder to BeagleBoard.
	2. insmod i2c_driver.ko
	2. chmod +x main_3 (optional if permision already present)
	3. ./main_3
				You will get a menu-based options.
				0: Write
						->Enter Number of Pages to write.
				1: Read
						->Enter Number of Pages to read.
				2: Seek
						->Enter Page number to seek.
	4. rmmod i2c_driver.ko


B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

######################################################################		
		#### Readme.txt for ESP Assignment-4 (Task 2) ####
######################################################################

Approach:
	1. I have created a ImpreciseComputation function which will give addition output of 1 to n numbers.
	2. I have registered a SIGALRM signal in main program.
	3. The signal will be raised after 1 second and intermediate output will be printed.


A) Steps to Execute the Program:
	1. Copy the main_4 file in Task2 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. ./main_4
				You will get a Computation Output after 1 Second. 
				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

######################################################################		
		#### Readme.txt for ESP Assignment-4 (Task 3) ####
######################################################################

Userspace Approach:
	1. I have created a ImpreciseComputation function which will give addition output of 1 to n numbers.
	2. Then, I created a thread program which checks for a double click event from Userspace.
	3. A signal handler for SIGTERM is registerd in main program.
	4. Whenever a double click event is received SIGTERM signal is raised. 
	3. The computation will stop and intermediate output will be printed.


A) Steps to Execute the Program:
	1. Copy the main_4 file in Task3 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. ./main_4
				You will get a Computation Output after Double Click. 
				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
		
######################################################################		
		#### Readme.txt for ESP Assignment-4 (Task 4) ####
######################################################################

Arbitrary Thread Approach:
	1. I have created 3 threads which can be increased as required.
	2. Main program will raise the first SIGALRM signal.
	3. This signal will be consumed by any one of the threads arbitrarily.
	4. The thread consuming a signal raises a new signal.
	5. The newly raised signal will again be consumed by any arbitrary thread.
	6. This goes in infinite loop.
	
A) Steps to Execute the Program:
	1. Copy the main_4 file in Task4/PART1 folder to BeagleBoard.
	2. chmod +x main_4 (optional if permision already present)
	3. ./main_4				
				

B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-

		
######################################################################		
		#### Readme.txt for ESP Assignment-5(Task 1) ####
######################################################################

Tested on : QEMU

Task 1:
	Changes are Made to Below Files:
		1. /FreeRTOS2/Source/portable/GCC/OMAP3_BeagleBoard/portISR.c
				- Added Interrupt handler for UART.
			
		2. /FreeRTOS2/Demo/Console/my_logger.c
				- New file created for logging printf messages.
				- Added support for handling different log levels.
						
		3. /FreeRTOS2/Demo/Console/include/my_logger.h
				- New file created to manage all declarations and config parameters.
		
		4. /FreeRTOS2/Demo/Console/printf.c
				- Modified slightly to manage both regular printf and Logging functionality.
				- Config Parameter defined in my_logger.h
		
		5. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/main.c
				- Added printf statements with diffrent Log Levels.
		
		6. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/Makefile
				- Modified slightly to handle newly added files.


Approach:
	1. Config Parameter "USE_DEFAULT_PRINTF" used to enable/disable logging functionality.
	2. Defined DEFAULT_MESSAGE_LOG_LEVEL and DEFAULT_CONSOLE_LOG_LEVEL config paramters.
	3. If logging functionality is enabled all printf messages will be stored in a log buffer and printf function will return immediately.
	4. UART Interrupt is enabled and character is printed on each interrupt.
	5. No Busy wait performed.
	6. Interrupt enable-disable and printf with logging enabled code added to my_logger.c file.				
		
Steps to compile and Execute:
		1. Go to /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC
		2. export PATH=/home/esp/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_EABI/bin/:$PATH
		3. execute: make clean.
		4. execute: make
		5. Go to path containing QEMU Image (beagle_sd.img)
		6. Execute: sudo mount -o loop,offset=$[63*512] beagle_sd.img /mnt/
		7. Execute: sudo cp /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/rtosdemo.bin /mnt/
		8. Execute: sudo umount /mnt.
		9. Execute: qemu-system-arm -M beaglexm -sd ./beagle_sd.img -clock unix -serial stdio
					mmc rescan 0
					fatload mmc 0 80300000 rtosdemo.bin
					go 0x80300000

					
######################################################################		
		#### Readme.txt for ESP Assignment-5(Task 2) ####
######################################################################
	
Tested on : QEMU

Note: Both Task-1 and Task-2 contain same code with different printf functionality.
	  However: Task 1 does everything using printf logging functionality (USE_DEFAULT_PRINTF = 0)
				Task 2 does everything using default printf. (USE_DEFAULT_PRINTF = 1)

	
Task 2:
	Changes Made in Below Files:
		1. /FreeRTOS2/Source/tasks.c   (Line No:427 to Line No:500)
				a) Added API to create Real-time Task.
				b) Added Real-time timer callback method.
				
 		2. /FreeRTOS2/Demo/SampleTasks/SampleTasks.c (Line No:113 to Line No:138)
				- Added Real-time Task function to execute.
				
		3. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/main.c 
				- Added code to create 10 real-time tasks, 1 critical and 1 lower priority task.
				
		4. /FreeRTOS2/Demo/Console/include/my_logger.h
				- Declared Global structures and Config parameters in one single .h file.
		
		5. /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/FreeRTOSConfig.h
				- Modified config parameter to allow 10 timers in FreeRTOS.
				
		6. /FreeRTOS2/Source/include/task.h
				- Declared Real-time Task creation API in this header file.
				

Approach:
	1. Created 10 Real-time tasks and associated a timer with each task.
	2. All real time tasks assigned priority 2.
	3. Created one critical task with priority 3 and one lower priority task with priority 1.
	4. Maintained a global Real-time timer task struct to keep track of all task deadlines.
	5. Added head and tail parameters to keep track of task overrun in-case unable to meet deadline.
	6. Timer used to poke each task at the start of its time period.
	7. Real time task suspended after finishing its execution and waits for next start period.
	8. Task with the highest priority executed at any time using default scheduling.
	9. If real time tasks with same priority arrive then task with earliest deadline executed first.
	
Steps to compile and Execute:
		1. Go to /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC
		2. export PATH=/home/esp/CodeSourcery/Sourcery_CodeBench_Lite_for_ARM_EABI/bin/:$PATH
		3. execute: make clean.
		4. execute: make
		5. Go to path containing QEMU Image (beagle_sd.img)
		6. Execute: sudo mount -o loop,offset=$[63*512] beagle_sd.img /mnt/
		7. Execute: sudo cp /FreeRTOS2/Demo/OMAP3_BeagleBoard_GCC/rtosdemo.bin /mnt/
		8. Execute: sudo umount /mnt.
		9. Execute: qemu-system-arm -M beaglexm -sd ./beagle_sd.img -clock unix -serial stdio
					mmc rescan 0
					fatload mmc 0 80300000 rtosdemo.bin
					go 0x80300000