/** Test progrom for the Driver - By Aniket Pansare **/
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int fd1;
	int res;
	char string2[256] = {'\0'};

	if ( argc < 2 ) /* argc should be 2 for correct execution */
    		{	
        	/* We print argv[0] assuming it is the program name */
        	printf( "usage: %s show\nusage: %s write \"<string>\"\n", argv[0],argv[0]);
    		}
    	else 
    		{
		 /* open devices */      
        	fd1 = open("/dev/My_Char_Device", O_RDWR);


        	if (fd1<0 )
        		{
                	printf("Can not open device file.. Please check if ran with sudo\n");
                	return 0;
        		}


        	if(!strcmp(argv[1],"show"))
			{
			res = read(fd1, string2, 256);
			if(res != 0)
				{
				printf("Can not read from the device file.\n");        
                                return 0;
				}
			printf("%s\n",string2);								
			}
		else if (!strcmp(argv[1],"write") && argc == 3)
			{ //For write check the 3rd argument
			res = write(fd1, argv[2], strlen(argv[2]));
			if(res !=0)
				{
                		printf("Can not write to the device file.\n");          
                		return 0;
        			}
			}
		else	{
			printf( "usage: %s show\nusage: %s write \"<string>\"\n", argv[0],argv[0]);
			}
	
		/* close devices */
		close(fd1);
		}
	
	return 0;
}

