/** Character Device Driver -by Aniket Pansare **/
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include<linux/fs.h>
#include<linux/jiffies.h>

#define DEVICE_NAME "My_Char_Device"
#define NAME_SIZE 20
#define ARR_SIZE 256

/** Function declarations **/
static int gmem_init(void);
static void gmem_exit(void);
static int gmem_open(struct inode *, struct file *); 
static ssize_t gmem_read(struct file *, char *, size_t, loff_t *);
static ssize_t gmem_write(struct file *, const char *, size_t, loff_t *);
static int gmem_release(struct inode *, struct file *);


/* Device structure */
struct My_dev {
	struct cdev cdev;               /* The cdev structure */
	char name[NAME_SIZE];           /* Name of device*/
	char charArray[ARR_SIZE];	/* buffer for the input string */
	int pos;			/* position of first '\0' */
} *my_devp;

static dev_t my_dev_number;      /* Allotted device number */
struct class *my_dev_class;      /* Tie with the device model */

/** file_operations structure **/
static struct file_operations fops = {
        .read = gmem_read,
        .open = gmem_open,
        .write = gmem_write,
	.release = gmem_release,
};

/** Function to Initailize the Driver **/
static int gmem_init(void) {
	int ret;

	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&my_dev_number, 0, 1, DEVICE_NAME) < 0) {
			printk(KERN_DEBUG "Can't register device\n"); return -1;
	}

	/* Populate sysfs entries */
	my_dev_class = class_create(THIS_MODULE, DEVICE_NAME);

	
	/* Allocate memory for the per-device structure */
	my_devp = kmalloc(sizeof(struct My_dev), GFP_KERNEL);

	/* Initialize character array and set the position of first \0 */
	sprintf(my_devp->charArray,"Hello world! This is Aniket, and this machine has worked for %lu seconds.",jiffies/HZ);
	my_devp->pos=0;
	while(my_devp->charArray[my_devp->pos]!='\0' && my_devp->pos < ARR_SIZE) my_devp->pos++;

	printk(KERN_INFO "Address of Jiffies: %p\n",&jiffies);	
	
	if (!my_devp) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}

	/* Request I/O region */
	sprintf(my_devp->name, DEVICE_NAME);


	/* Connect the file operations with the cdev */
	cdev_init(&my_devp->cdev, &fops);
	my_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
	ret = cdev_add(&my_devp->cdev, (my_dev_number), 1);

	if (ret) {
		printk("Bad cdev\n");
		return ret;
	}

	/* Send uevents to udev, so it'll create /dev nodes */
	device_create(my_dev_class, NULL, MKDEV(MAJOR(my_dev_number), 0), NULL, DEVICE_NAME);		
	

	printk("My Driver Initialized.\n");
	return 0;

}

/** Exit Driver **/
static void gmem_exit(void) {
	/* Release the major number */
	unregister_chrdev_region((my_dev_number), 1);

	/* Destroy device */
	device_destroy (my_dev_class, MKDEV(MAJOR(my_dev_number), 0));
	cdev_del(&my_devp->cdev);
	kfree(my_devp);
	
	/* Destroy driver_class */
	class_destroy(my_dev_class);

	printk("My Driver removed.\n");
}

/** Open the Driver **/
static int gmem_open(struct inode *inode, struct file *file) {
	struct My_dev *my_devp;

	/* Get the per-device structure that contains this cdev */
	my_devp = container_of(inode->i_cdev, struct My_dev, cdev);

	/* Easy access to cmos_devp from rest of the entry points */
	file->private_data = my_devp;

	printk("\n%s is opening\n", my_devp->name);
	return 0;
}

/*
 * Release the driver
 */
static int gmem_release(struct inode *inode, struct file *file)
{
	struct My_dev *my_devp = file->private_data;
	
	printk("\n%s is closing\n", my_devp->name);
	
	return 0;
}


/** Read from the Driver **/
static ssize_t gmem_read(struct file *fp,char *buff,size_t len,loff_t *offset) {
	int pos = 0,res=0;
	printk(KERN_INFO "Driver: read()\n");
	while(len >0 && my_devp->charArray[pos]!='\0' && res==0)
	{
		//*buff++=charArray[pos++];
		res=copy_to_user((void __user *)buff++, (void *)&my_devp->charArray[pos++], 1);		
		len--;
	}
	*buff='\0';
	return res;
}


/** Write to the Driver **/
static ssize_t gmem_write(struct file *fp,const char *buff,size_t len,loff_t *off) {
	int pos=0, res=0;
	printk(KERN_INFO "Driver: write()\n");

	/**Get the position of the first '\0' **/	
	pos=my_devp->pos;	

	while(len > 0 && *buff!='\0' && res==0)
	{
		pos = pos % 256;
		res=copy_from_user((void *)&my_devp->charArray[pos++], (void __user *)buff++, 1);
		len--; 
	}
	pos = pos % 256;
	my_devp->pos=pos;
	my_devp->charArray[pos]='\0';
	return res;
}

module_init(gmem_init);
module_exit(gmem_exit);

MODULE_AUTHOR("Aniket Pansare");
MODULE_DESCRIPTION("Character Decice Driver");
MODULE_LICENSE("GPL");
