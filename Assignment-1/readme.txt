A) Steps to Execute the Program:
	1. Copy the gmem.ko and the gmem_tester file to BeagleBoard.
	2. insmod gmem.ko
	3. chmod +x gmem_tester (optional if permision already present)
	4. ./gmem_tester show
	5. ./gmem_tester write "String to append"


B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
