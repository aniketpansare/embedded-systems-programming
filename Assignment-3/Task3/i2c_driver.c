/** ESP Assignment 3 : Task 2 (Custom I2C Driver for EEPROM) **/
/*  Author: Aniket Pansare  */

#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/fs.h>
#include <linux/semaphore.h>
#include <linux/i2c.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/workqueue.h>

//Macros
#define DRIVER_NAME "i2c_flash"
#define NAME_SIZE 20
#define I2C_ADDR 0x52
#define I2C_PAGE_SIZE 64
#define MAX_PAGES 512

//Declare driver functions
int i2c_open(struct inode *,struct file * );
ssize_t i2c_read(struct file *, char __user *,size_t , loff_t *);
ssize_t i2c_write(struct file * , const char *, size_t , loff_t *);
loff_t i2c_llseek(struct file * , loff_t  , int );
int i2c_release(struct inode *, struct file *);

/* My I2C Device structure */
struct My_dev {
        struct cdev cdev;               /* The cdev structure */
        char name[NAME_SIZE];           /* Name of device*/
	struct i2c_client *i2c_client_ptr;
	int i2c_cur_page;
	char high_address;
	char low_address;
} *my_devp;

void i2c_get_cur_address(struct My_dev *);

static dev_t my_dev_number;      /* Allotted device number */
struct class *my_dev_class;      /* Tie with the device model */

/* EEPROM States */
enum i2c_state 
{
	READY,
	BUSY,
	DONE
}i2c_state;


/* I2C Work Struct*/
typedef struct 
{
	struct work_struct work;
	struct My_dev *my_devptr;
	int page_count;
	int i2c_rw_flag;
}i2c_work;

i2c_work *i2c_rwork, *i2c_wwork;

/*I2C Work Queue*/
static struct workqueue_struct *i2c_work_queue;

/*Read and Write Buffers*/
char *read_buf,*write_buf;

/** file_operations structure **/
static struct file_operations fops = {
       	.owner=THIS_MODULE,
	.read = i2c_read,
	.write = i2c_write,
        .open = i2c_open,
	.llseek = i2c_llseek,
        .release = i2c_release,
};

/**
* EEPROM Read/ Write  work function.
*/
static void work_func(struct work_struct *work_ptr)
{
    char addr_buf[2];
    char data[66], *i2c_buf; 

    int i;

    i2c_work *i2c_work_ptr = (i2c_work *)work_ptr;

    if(((i2c_work *)work_ptr)->i2c_rw_flag == 0)  /** Read Call **/
    {
         //Allocate Memory for read buffer.
	 read_buf = (char *)kmalloc(sizeof(char)*(i2c_work_ptr->page_count)*I2C_PAGE_SIZE, GFP_KERNEL);

	 //Get Current Page address. 
         i2c_get_cur_address(i2c_work_ptr->my_devptr);
         addr_buf[0] = i2c_work_ptr->my_devptr->high_address;
         addr_buf[1] = i2c_work_ptr->my_devptr->low_address;

	 //Write to EEPROM the page address to read.
         i2c_master_send(i2c_work_ptr->my_devptr->i2c_client_ptr, addr_buf, 2);
	 //Read from EEPROM.
         i2c_master_recv(i2c_work_ptr->my_devptr->i2c_client_ptr, read_buf, (i2c_work_ptr->page_count)*I2C_PAGE_SIZE);

	 //Set Current Page after read.
         i2c_work_ptr->my_devptr->i2c_cur_page = (i2c_work_ptr->my_devptr->i2c_cur_page + i2c_work_ptr->page_count) % MAX_PAGES;

    }
    else
    {
       i2c_buf = write_buf;
       for(i=0; i<(i2c_work_ptr->page_count); i++)
       {
          //Get Current Page address;
	  i2c_get_cur_address(i2c_work_ptr->my_devptr);
          data[0] = i2c_work_ptr->my_devptr->high_address;
          data[1] = i2c_work_ptr->my_devptr->low_address;

	  //Append data to write. 
          memcpy((char *)data+2, write_buf,I2C_PAGE_SIZE);

	  //Write data to EEPROM
          i2c_master_send(i2c_work_ptr->my_devptr->i2c_client_ptr, data, I2C_PAGE_SIZE + 2);
          msleep(1);
      
	  write_buf += I2C_PAGE_SIZE;
	  //Loop over if Max Pages exceeded. 
          i2c_work_ptr->my_devptr->i2c_cur_page = (i2c_work_ptr->my_devptr->i2c_cur_page+1) % MAX_PAGES;
       }
       
       write_buf = i2c_buf;

    }

   i2c_state = DONE;
}

/**
* Get current page address in EEPROM.
* @param My_dev struct pointer.
*/ 
void i2c_get_cur_address(struct My_dev *my_devptr)
{
	my_devptr->high_address =((my_devptr->i2c_cur_page << 6) & 0xFF00) >> 8;
	my_devptr->low_address = ((my_devptr->i2c_cur_page << 6) & 0x00FF);
}


/**
* Init Function.
*/
int __init i2c_init(void) {
        int ret;

	struct i2c_adapter *adapter;
	struct i2c_board_info board_info;
	unsigned short const addr_list[2]={I2C_ADDR,I2C_CLIENT_END};

        /* Request dynamic allocation of a device major number */
        if (alloc_chrdev_region(&my_dev_number, 0, 1, DRIVER_NAME) < 0) {
                        printk(KERN_DEBUG "Can't register device\n"); return -1;
        }

        /* Populate sysfs entries */
	my_dev_class = class_create(THIS_MODULE, DRIVER_NAME);

	/* Allocate memory for the per-device structure */
	my_devp = kmalloc(sizeof(struct My_dev), GFP_KERNEL);

	if (!my_devp) {
                        printk("Bad Kmalloc\n"); return -ENOMEM;
        }

	/* Connect the file operations with the cdev */
	cdev_init(&my_devp->cdev, &fops);
        my_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
        ret = cdev_add(&my_devp->cdev,MKDEV(MAJOR(my_dev_number),0),1);

	if (ret) {
                        printk("Device could not be added.\n");
                        return ret;
        }

	//Send uevents to udev, so it'll create /dev nodes */
        device_create(my_dev_class, NULL, MKDEV(MAJOR(my_dev_number), 0), NULL, DRIVER_NAME);

	/*Init page number to 0 */
	my_devp->i2c_cur_page = 0;

	/*Get I2C Adapter*/
	adapter=i2c_get_adapter(2);
	
	if(!adapter)
	{
		printk("\n!!! Unable to get I2C Adapter !!!");
		return -1;
	}

	memset(&board_info,0,sizeof(struct i2c_board_info));	

	/*Initialize I2C client*/
	my_devp->i2c_client_ptr = i2c_new_probed_device(adapter,&board_info,addr_list,NULL);

	if(!my_devp->i2c_client_ptr)
	{
		printk("\n!!! I2C Client Initialization failed !!!");
		return -1;
	}

	/*Work Queue*/
	i2c_work_queue = create_workqueue("i2c_queue");
	i2c_state = READY;

	printk("My Driver Initialized: %s",my_devp->name);
        return 0;	

}

/** Exit Driver **/
void __exit i2c_exit(void) {

        /* Release the major number */
        unregister_chrdev_region((my_dev_number), 1);

	/*Unregister Device*/
	i2c_unregister_device(my_devp->i2c_client_ptr);

        /* Destroy device */
        device_destroy (my_dev_class, MKDEV(MAJOR(my_dev_number), 0));
        cdev_del(&my_devp->cdev);
        kfree(my_devp);

        /* Destroy driver_class */
        class_destroy(my_dev_class);

	flush_workqueue(i2c_work_queue);
   	destroy_workqueue(i2c_work_queue);

        printk("I2C Driver removed.\n");

}

/**
* Open Device.
* @param struct inode *
* @param struct file *
*/
int i2c_open(struct inode *inode, struct file *filep){
	
	struct My_dev *my_devptr  = (void *)container_of(inode->i_cdev,struct My_dev,cdev);
	
	filep->private_data = (void *) my_devptr;

	printk("\n!!! Device opened : %s !!!",my_devptr->name);

	return 0;
}

/**
* Release device.
* @param struct inode *
* @param struct file *
*/
int i2c_release(struct inode *inode, struct file *filep){

	struct My_dev *my_devptr = (struct My_dev *) filep->private_data;	

	printk("\n!!! I2C Driver Released: %s !!!\n",my_devptr->name);

	return 0;
}

/**
* Read from EEPROM.
* @param struct file *
* @param char __user *buf : Read into user buffer.
* @param size_t count : no of pages to read.
* @param loff_t
*/
ssize_t i2c_read(struct file *filep, char __user *buf, size_t count,loff_t *pos)
{

	int ret;
	
	struct My_dev *my_devptr = filep->private_data;

	switch(i2c_state)
	{
		case DONE: //IF read done.. Copy buffer to User Space and reset EEPROM state.
			ret = copy_to_user((void __user *)buf, (void *) read_buf, count * I2C_PAGE_SIZE);		
			i2c_state = READY;
			kfree(read_buf);
			kfree(i2c_rwork);
			return 0;
			break;

		case BUSY: //IF EEPROM Busy.
			return 1;
			break;
		
		default:  //IF EEPROM Available enqueue in work Queue.
			i2c_rwork = (i2c_work *)kmalloc(sizeof(i2c_work),GFP_KERNEL);
			if(i2c_rwork)
			{
				i2c_rwork->i2c_rw_flag = 0;
           			i2c_state = READY;
            			i2c_rwork->my_devptr = my_devptr ;

				/*Initialize work Structure.. Bind work_func() to the structure*/	
            			INIT_WORK((struct work_struct *)i2c_rwork, work_func);
			
			}
			else
        		{
             			printk("\n!! Memory allocation Failed : Read Operation !!"); 
        		}

        		i2c_rwork->page_count = count;
			/*Enqueue the initialized work structure*/
        		
			queue_work(i2c_work_queue, (struct work_struct *)i2c_rwork);
        		i2c_state = BUSY;
       	 		return 2;
			break;


	}

	return 0;
}

/**
* Write to EEPROM.
* @param struct file *
* @param char __user *buf : Buffer to write from
* @param size_t count : No of pages to write
* @param loff_t
*/
ssize_t i2c_write(struct file *filep, const char * buf, size_t count, loff_t *pos)
{
	int ret;

	struct My_dev *my_devptr = filep->private_data;

	switch(i2c_state)
	{

		case DONE: //Write Done.
			i2c_state = READY;
			kfree(write_buf);
			kfree(i2c_wwork);
			return 0;
			break;

		case BUSY: //If Busy return.
			return 1;
	
		default:
			//Allocate memory for write work structure.
        		i2c_wwork = (i2c_work *)kmalloc(sizeof(i2c_work),GFP_KERNEL);
      			
			if(i2c_wwork)
        		{
          			 i2c_state = READY;
          			 i2c_wwork->i2c_rw_flag = 1;
          			 i2c_wwork->my_devptr = my_devptr ;

            			INIT_WORK((struct work_struct *)i2c_wwork, work_func);
       			 }
       			 else
       			 {
            			 printk("\n!! Memory Allocation Failed : Write Operation !!"); 
       			 }

       			 i2c_wwork->page_count = count;

			 //Allocate memory for write buffer;
       			 write_buf = kmalloc(count*I2C_PAGE_SIZE, GFP_KERNEL);
			 //Copy Write buffer from user space to kernel space
       			 ret = copy_from_user( (void *)write_buf, (void __user *)buf, count*I2C_PAGE_SIZE);

       			 /*Enqueue in work queue*/
			 queue_work(i2c_work_queue, (struct work_struct *)i2c_wwork);
       			 i2c_state = BUSY;
			 return 2;	
	}
	
//	return ret;
}

/**
* Seek in EEPROM.
* @param struct file *
* @param loff_t offset : Page number to seek.
* @param whence
*/
loff_t i2c_llseek(struct file *filep, loff_t offset, int whence)
{
	struct My_dev *my_devptr = filep->private_data;

	switch(whence)
	{
		case 0:
			if( offset >= MAX_PAGES)
				return -1;

			my_devptr->i2c_cur_page = offset;
			i2c_get_cur_address(my_devptr);
		break;
		
		default:
			return -EINVAL;
	}

	return 0;
}


module_init(i2c_init);
module_exit(i2c_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Aniket Pansare");


