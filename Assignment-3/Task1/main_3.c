/**  ESP Assignment 3: Test i2c-2 device driver for EEPROM**/
/**  Author: Aniket pansare */

#include<stdio.h>
#include<time.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#include<sys/ioctl.h>
#include<sys/types.h>

#define WRITE 0
#define READ 1
#define SEEK 2
#define EXIT 3
#define MAX_PAGES 512
#define PAGE_SIZE 64

int fd;
int cur_page_no=0; //Current Page.  
char page_addr[2]; //Current Page address (High / low byte)

/**
* Print bytes in the buffer.
* @param character buffer.
* @param Number of bytes to print.
*/
void print_bytes(char *buf, int count )
{
        int i=0;

        for(i=0; i<count; i++)
        {
                printf("%d\n",(int) buf[i]);
        }

}


/**
* Main Program.
*/
int main()
{
	int ret=0,choice=0, i=0;
	char* writebuf, * readbuf;
	int count=0;
	int no_of_pages = 0;

	printf("### ESP Assignment 3: Task 1 ###\n");

	//Open I2C Device Driver.
	fd = open("/dev/i2c-2", O_RDWR);
	
	if(fd<0)
	{
		printf("!!!! Cannot Open the I2C Device File !!!!\n");
		return 0;
	}

	//Set EEPROM
	ret= ioctl(fd,0x703,0x52);
	if(ret<0)
	{
		printf("!!!! IOCTL ERROR !!!!");
		return 0;
	}
	
	while(1)
	{
	printf("Enter your choice:\n 0 => Write \n 1 => Read \n 2 => Seek \n 3 => Exit.");
	scanf("%d",&choice);

	switch(choice)
	{

	case WRITE:
		printf("\nEnter no of pages to write to EEPROM:");
                scanf("%d",&no_of_pages);

                if(no_of_pages >= MAX_PAGES || no_of_pages <0)
                {
                        printf("Invalid number of pages to read.");
                        return -1;
                }
		
		writebuf = (char *) malloc(PAGE_SIZE*no_of_pages);
		
		if(writebuf == NULL)
		{		
			printf("Memory allocation failed for write buffer \n");
			return -1;
		}

		for(i =0; i < PAGE_SIZE*no_of_pages; i++)
		{
			writebuf[i]= (char) i; //rounds up if its exceeds 256.
		}

		write_EEPROM(writebuf,no_of_pages);
		free(writebuf);
		break;

	case READ:
		printf("\nEnter no of pages to read from EEPROM:");
		scanf("%d",&no_of_pages);
		
		if(no_of_pages >= MAX_PAGES || no_of_pages < 0)
		{
			printf("Invalid number of pages to read.");
			return -1;
		}

		readbuf= (char*) malloc(PAGE_SIZE*no_of_pages);

		if(readbuf == NULL)    
                {
                        printf("Memory allocation failed for read buffer \n");
                        return -1;
                }
				
		read_EEPROM(readbuf,no_of_pages);
		free(readbuf);
		break;

	case SEEK:
		printf("\nEnter page no to seek in EEPROM:");
		int page_to_seek=0;
		scanf("%d",&page_to_seek);	
		if(page_to_seek < 0 || page_to_seek > 511)
		{
			printf("\nInvalid Page Number.. Please Enter Range (0-511)");
			break;
		}	

		seek_EEPROM(page_to_seek);
		break;

	case EXIT:
		return 0;

	}

      }
	
   return ret;	

}

/**
* Write to EEPROM.
* @param Character buffer to write from.
* @param No of pages to write.
*/
int write_EEPROM(const char *buf, int count)
{
   int ret=0,page=0;
   char data[PAGE_SIZE+2];

   for(page=0;page<count;page++)
	{
		seek_EEPROM(cur_page_no);	
		//set page address in first two bytes.	
		memcpy(data,page_addr,2);
		memcpy(&data[2],&buf[page*PAGE_SIZE],PAGE_SIZE);		
	
		// write one page at a time.
		ret=write(fd,data,PAGE_SIZE+2);

		sleep(1);
		printf("Written Page: %d\n",cur_page_no);
		cur_page_no = (cur_page_no+1)%MAX_PAGES;
	}

    return ret;

}

/**
* Read from EEPROM.
* @param Character buffer to read into.
* @param No of pages to read.
*/
int read_EEPROM(char *buf, int count)
{
	int ret =0;
	int page =0;

	seek_EEPROM(cur_page_no);
	
	ret=write(fd,page_addr,2);
	//Read Pages in to the buffer.
	ret = read(fd,buf,PAGE_SIZE * count);

	print_bytes(buf,PAGE_SIZE*count);

	cur_page_no = (cur_page_no +count) % MAX_PAGES;	
	return ret;
}

/**
* Seek EEPROM.
* @param Page No to seek.
*/
int seek_EEPROM(int offset)
{
	int ret=0;
	cur_page_no=offset;
	
	if(offset >= MAX_PAGES)
		return -1;

	page_addr[1] = ((offset << 6) & 0x00ff);  //Lower byte of page address.
	page_addr[0] = ((offset << 6) & 0xff00 >> 8);  //Higher byte of page address. 	

	return ret;
}
