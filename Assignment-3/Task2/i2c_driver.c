/** ESP Assignment 3 : Task 2 (Custom I2C Driver for EEPROM) **/
/*  Author: Aniket Pansare  */

#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/fs.h>
#include <linux/semaphore.h>
#include <linux/i2c.h>
#include <linux/time.h>
#include <linux/delay.h>

//Macros
#define DRIVER_NAME "i2c_flash"
#define NAME_SIZE 20
#define I2C_ADDR 0x52
#define I2C_PAGE_SIZE 64
#define MAX_PAGES 512

//Declare driver functions
int i2c_open(struct inode *,struct file * );
ssize_t i2c_read(struct file *, char __user *,size_t , loff_t *);
ssize_t i2c_write(struct file * , const char *, size_t , loff_t *);
loff_t i2c_llseek(struct file * , loff_t  , int );
int i2c_release(struct inode *, struct file *);

/* My I2C Device structure */
struct My_dev {
        struct cdev cdev;               /* The cdev structure */
        char name[NAME_SIZE];           /* Name of device*/
	struct i2c_client *i2c_client_ptr;
	int i2c_cur_page;
	char high_address;
	char low_address;
} *my_devp;

void i2c_get_cur_address(struct My_dev *);

static dev_t my_dev_number;      /* Allotted device number */
struct class *my_dev_class;      /* Tie with the device model */

/** file_operations structure **/
static struct file_operations fops = {
       	.owner=THIS_MODULE,
	.read = i2c_read,
	.write = i2c_write,
        .open = i2c_open,
	.llseek = i2c_llseek,
        .release = i2c_release,
};

/**
* Get current page address in EEPROM.
* @param My_dev struct pointer.
*/ 
void i2c_get_cur_address(struct My_dev *my_devptr)
{
	my_devptr->high_address = ((my_devptr->i2c_cur_page << 6) & 0xff00) >> 8;
	my_devptr->low_address =  ((my_devptr->i2c_cur_page << 6) & 0x00ff);
}


/**
* Init Function.
*/
int __init i2c_init(void) {
        int ret;

	struct i2c_adapter *adapter;
	struct i2c_board_info board_info;
	unsigned short const addr_list[2]={I2C_ADDR,I2C_CLIENT_END};

        /* Request dynamic allocation of a device major number */
        if (alloc_chrdev_region(&my_dev_number, 0, 1, DRIVER_NAME) < 0) {
                        printk(KERN_DEBUG "Can't register device\n"); return -1;
        }

        /* Populate sysfs entries */
	my_dev_class = class_create(THIS_MODULE, DRIVER_NAME);

	/* Allocate memory for the per-device structure */
	my_devp = kmalloc(sizeof(struct My_dev), GFP_KERNEL);

	if (!my_devp) {
                        printk("Bad Kmalloc\n"); return -ENOMEM;
        }

	/* Connect the file operations with the cdev */
	cdev_init(&my_devp->cdev, &fops);
        my_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
        ret = cdev_add(&my_devp->cdev,MKDEV(MAJOR(my_dev_number),0),1);

	if (ret) {
                        printk("Device could not be added.\n");
                        return ret;
        }

	//Send uevents to udev, so it'll create /dev nodes */
        device_create(my_dev_class, NULL, MKDEV(MAJOR(my_dev_number), 0), NULL, DRIVER_NAME);

	my_devp->i2c_cur_page=0;

	/*Get I2C Adapter*/
	adapter=i2c_get_adapter(2);
	
	if(!adapter)
	{
		printk("\n!!! Unable to get I2C Adapter !!!");
		return -1;
	}

	memset(&board_info,0,sizeof(struct i2c_board_info));	

	/*Initialize I2C client*/
	my_devp->i2c_client_ptr = i2c_new_probed_device(adapter,&board_info,addr_list,NULL);

	if(!my_devp->i2c_client_ptr)
	{
		printk("\n!!! I2C Client Initialization failed !!!");
		return -1;
	}

	printk("My Driver Initialized: %s",my_devp->name);
        return 0;	

}

/** Exit Driver **/
void __exit i2c_exit(void) {


        /* Release the major number */
        unregister_chrdev_region((my_dev_number), 1);

	/*Unregister Device*/
	i2c_unregister_device(my_devp->i2c_client_ptr);

        /* Destroy device */
        device_destroy (my_dev_class, MKDEV(MAJOR(my_dev_number), 0));
        cdev_del(&my_devp->cdev);
        kfree(my_devp);

        /* Destroy driver_class */
        class_destroy(my_dev_class);

        printk("I2C Driver removed.\n");

}

/**
* Open Device.
* @param struct inode *
* @param struct file *
*/
int i2c_open(struct inode *inode, struct file *filep){
	
	struct My_dev *my_devptr  = (void *)container_of(inode->i_cdev,struct My_dev,cdev);
	
	filep->private_data = (void *) my_devptr;

	printk("\n!!! Device opened : %s !!!",my_devptr->name);

	return 0;
}

/**
* Release device.
* @param struct inode *
* @param struct file *
*/
int i2c_release(struct inode *inode, struct file *filep){

	struct My_dev *my_devptr = (struct My_dev *) filep->private_data;	

	printk("\n!!! I2C Driver Released: %s !!!\n",my_devptr->name);

	return 0;
}

/**
* Read from EEPROM.
* @param struct file *
* @param char __user *buf : Read into user buffer.
* @param size_t count : no of pages to read.
* @param loff_t
*/
ssize_t i2c_read(struct file *filep, char __user *buf, size_t count,loff_t *pos)
{

	char *i2c_buf,i2c_addr[2];
	int ret;
	
	struct My_dev *my_devptr = filep->private_data;

	i2c_buf = kmalloc(sizeof(char)* count * I2C_PAGE_SIZE, GFP_KERNEL);

	i2c_get_cur_address(my_devptr);

	i2c_addr[0] = my_devptr->high_address;
	i2c_addr[1] = my_devptr->low_address;

	i2c_master_send(my_devptr->i2c_client_ptr,i2c_addr,2);
	i2c_master_recv(my_devptr->i2c_client_ptr,i2c_buf,count * I2C_PAGE_SIZE);

	ret = copy_to_user((void __user *)buf, (void *) i2c_buf, count * I2C_PAGE_SIZE);

	my_devptr->i2c_cur_page = (my_devptr->i2c_cur_page + count)%MAX_PAGES;
	kfree(i2c_buf);

	return 0;
}

/**
* Write to EEPROM.
* @param struct file *
* @param char __user *buf : Buffer to write from
* @param size_t count : No of pages to write
* @param loff_t
*/
ssize_t i2c_write(struct file *filep, const char * buf, size_t count, loff_t *pos)
{
	char *i2c_buf;
	char data[66],*temp;
	int page, ret;

	struct My_dev *my_devptr = filep->private_data;

	i2c_buf = kmalloc(sizeof(char)* count * I2C_PAGE_SIZE,GFP_KERNEL);


	ret = copy_from_user((void *) i2c_buf, (void __user *)buf, count*I2C_PAGE_SIZE); 
	temp = i2c_buf;
	
	for(page=0; page<count; page++)
	{
		
		i2c_get_cur_address(my_devptr);
		data[0] = my_devptr->high_address;
        	data[1] = my_devptr->low_address;
	
		memcpy((char *)data + 2,i2c_buf,I2C_PAGE_SIZE);
		
		i2c_master_send(my_devptr->i2c_client_ptr, data , I2C_PAGE_SIZE +2);
		
		msleep(1);

		my_devptr->i2c_cur_page = (my_devptr->i2c_cur_page +1) % MAX_PAGES;
		i2c_buf = i2c_buf + I2C_PAGE_SIZE;	

	}

	i2c_buf = temp;
	kfree(i2c_buf);
	
	return 0;
}

/**
* Seek in EEPROM.
* @param struct file *
* @param loff_t offset : Page number to seek.
* @param whence
*/
loff_t i2c_llseek(struct file *filep, loff_t offset, int whence)
{
	struct My_dev *my_devptr = filep->private_data;

	switch(whence)
	{
		case 0:
			if(offset >= MAX_PAGES || offset < -1)
			{
				return -1;
			}
			my_devptr->i2c_cur_page = offset;
			i2c_get_cur_address(my_devptr);
		break;
		
		default:
			return -1;
	}

	return 0;
}


module_init(i2c_init);
module_exit(i2c_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Aniket Pansare");


