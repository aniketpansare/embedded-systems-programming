######### Assignment-3 (Task-2) #####################

A) Steps to Execute the Program:
	1. Copy the i2c_driver.ko and main_3 file in Task2 folder to BeagleBoard.
	2. insmod i2c_driver.ko
	2. chmod +x main_3 (optional if permision already present)
	3. ./main_3
				You will get a menu-based options.
				0: Write
						->Enter Number of Pages to write.
				1: Read
						->Enter Number of Pages to read.
				2: Seek
						->Enter Page number to seek.
	4. rmmod i2c_driver.ko


B) Makefile can be executed if incompatible kernel version:
	1. Download source code of the kernel and toolchain as instructed in lab Manual Part 1
	2. Execute makefile:
		make ARCH=ARM CROSS_COMPILE=arm-angstrom-linux-gnueabi-
