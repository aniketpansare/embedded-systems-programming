/**  ESP Assignment 3- Test Custom I2C_Flash Driver **/
/*   Author - Aniket Pansare	*/

#include<stdio.h>
#include<time.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#include<sys/ioctl.h>
#include<sys/types.h>

#define WRITE 0
#define READ 1
#define SEEK 2
#define EXIT 3
#define MAX_PAGES 512
#define PAGE_SIZE 64

int fd;

/**
* Prints the bytes in the buffer.
* @param character buffer.
* @param no of bytes to print.
*/
void print_bytes(char *buf, int count )
{
        int i=0;

        for(i=0; i<count; i++)
        {
                printf("%d\n",(int) buf[i]);
        }

}

/**
* Main function
*/
int main()
{
	int ret=0,choice=0, i=0;
	char* writebuf, * readbuf;
	int count=0;
	int no_of_pages = 0;

	printf("### ESP Assignment 3 ###\n");

	fd = open("/dev/i2c_flash", O_RDWR);
	
	if(fd<0)
	{
		printf("!!!! Cannot Open the I2C Device File !!!!\n");
		return 0;
	}

	while(1)
	{
	printf("Enter your choice:\n 0 => Write \n 1 => Read \n 2 => Seek \n 3 => Exit.");
	scanf("%d",&choice);

	switch(choice)
	{

	case WRITE:
		printf("\nEnter no of pages to write to EEPROM:");
                scanf("%d",&no_of_pages);

                if(no_of_pages >= MAX_PAGES || no_of_pages <0)
                {
                        printf("Invalid number of pages to read.");
                        return -1;
                }
		
		writebuf = (char *) malloc(PAGE_SIZE*no_of_pages);
		
		if(writebuf == NULL)
		{		
			printf("\n!! Memory allocation failed for write buffer !!\n");
			return -1;
		}

		for(i =0; i < PAGE_SIZE*no_of_pages; i++)
		{
			writebuf[i]= (char) i; //rounds up if its exceeds 256.
		}

		write(fd,writebuf,no_of_pages);
		free(writebuf);
		break;

	case READ:
		printf("\nEnter no of pages to read from EEPROM:");
		scanf("%d",&no_of_pages);
		
		if(no_of_pages >= MAX_PAGES || no_of_pages < 0)
		{
			printf("Invalid number of pages to read.");
			return -1;
		}

		readbuf= (char*) malloc(PAGE_SIZE*no_of_pages);

		if(readbuf == NULL)    
                {
                        printf("\n!! Memory allocation failed for read buffer !!\n");
                        return -1;
                }
				
		read(fd, readbuf,no_of_pages);
		print_bytes(readbuf,no_of_pages*PAGE_SIZE);
		free(readbuf);
		break;

	case SEEK:
		printf("\nEnter page no to seek in EEPROM:");
		int page_to_seek=0;
		scanf("%d",&page_to_seek);
		if(page_to_seek < 0 || page_to_seek > 511)
		{
			printf("\n Invalid Page Number. Please enter range (0-511)");
			break;
		}		

		lseek(fd,page_to_seek,0);
		break;

	case EXIT:
		return 0;

	}

      }
	
   return ret;	

}

